# Processo e Sviluppo del Software: Assignment 3 - Movies


## Team members
- Gianluca Giudice - **830694**
- Marco Grassi - **829664**

## Gitlab repository
https://gitlab.com/gianlucagiudice/2020_assignment3_movies

## Installation

### Requirements
- Java `11`
- OpenJDK `11`
- Apache Maven `3.6.3` 
- Docker `20.10.0`

For tests purpose we use a docker container running postgres.


## Execution
Execute:
```
$ cd 2020_assignment3_movies
$ docker run -d --name postgres-tests \
    -e POSTGRES_PASSWORD=admin \
    -p 5432:5432 postgres:alpine
$ docker exec -it postgres-tests psql -U postgres -c 'CREATE DATABASE movies_test;'

$ # Run tests
$ mvn clean
$ mvn verify
```
This script will perform the following operations:
1. Start a container with postgres
2. Run tests using maven
   
## The app
Movies is an application written in Java using the
SpringBoot Framework and JPA with postgres as database. The following image represents the ER diagram:

![ER diagram](images/er-concettuale.png)

## Usage
The application exposes an API with these endpoints:

**Base URL**: localhost:8080/api/

### CRUD operations
#### Actor

| Request type | Endpoint                      | Description                                                                                            |
|:------------:|:------------------------------|--------------------------------------------------------------------------------------------------------|
| `GET`        | **/actors**                   | Get all the actors                                                                                     |
| `GET`        | **/actors/{id}**              | Get an actor given the ID                                                                              |
| `GET`        | **/actors/surname/{surname}** | Find the actors given the surname                                                                      |
| `POST`       | **/actors**                   | Add a new actor. A JSON object representing the new actor must be present in the POST request body     |
| `PUT`        | **/actors/{id}**              | Update actor with ID. A JSON object representing the new actor must be present in the PUT request body |
| `DELETE`     | **/actors/{id}**              | Delete an actor given the ID                                                                           |
    
#### Director 
    
| Request type | Endpoint                         | Description                                                                                                  |
|:------------:|:---------------------------------|--------------------------------------------------------------------------------------------------------------|
| `GET`        | **/directors**                   | Get all the directors                                                                                        |
| `GET`        | **/directors/{id}**              | Get a director given the ID                                                                                  |
| `GET`        | **/directors/surname/{surname}** | Find the directors given the surname                                                                         |
| `POST`       | **/directors**                   | Add a new director. A JSON object representing the new director must be present in the POST request body     |
| `PUT`        | **/directors/{id}**              | Update director with ID. A JSON object representing the new director must be present in the PUT request body |
| `DELETE`     | **/directors/{id}**              | Delete a director given the ID                                                                               |
    
#### Publisher
| Request type | Endpoint                       | Description                                                                                                    |
|:------------:|:-------------------------------|----------------------------------------------------------------------------------------------------------------|
| `GET`        | **/publishers**                | Get all the publishers                                                                                         |
| `GET`        | **/publishers/{id}**           | Get a publisher given the ID                                                                                   |
| `GET`        | **/publishers/name/{surname}** | Find the publisher given the name                                                                              |
| `POST`       | **/publishers**                | Add a new publisher. A JSON object representing the new publisher must be present in the POST request body     |
| `PUT`        | **/publishers/{id}**           | Update publisher with ID. A JSON object representing the new publisher must be present in the PUT request body |
| `DELETE`     | **/publishers/{id}**           | Delete a publisher given the ID                                                                                |

#### Film

| Request type | Endpoint                                                               | Description                                                                                             |
|:------------:|:-----------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| `GET`        | **/films**                                                             | Get all the films                                                                                       |
| `GET`        | **/films/{id}**                                                        | Get the film by the ID                                                                                  |
| `GET`        | **/films/title/{title}**                                               | Find the film given the title                                                                           |
| `POST`       | **/films**                                                             | Add a new film. A JSON object representing the new film must be present in the POST request body        |
| `PUT`        | **/films/{id}**                                                        | Update film with ID. A JSON object representing the new film must be present in the PUT request body    |
| `PUT`        | **/films/{filmId}/sequel/{sequelId}**                                  | Set the sequel to the film given by ID. The sequel is represented by the ID as well                     |
| `PUT`        | **/films/{filmId}/prequel/{prequelId}**                                | Set the prequel to the film given by ID. The prequel is represented by the ID as well                   |
| `DELETE`     | **/films/{filmId}/sequel**                                             | Unset the sequel of the film given by ID.                                                               |
| `DELETE`     | **/films/{filmId}/prequel**                                            | Unset the prequel of the film given by ID.                                                              |
| `PUT`        | **/films/{filmId}/actor/{actorId}**                                    | Add the relation between the film given by the ID and the actor given by ID                             |
| `DELETE`     | **/films/{filmId}/actor/{actorId}**                                    | Delete the relation between the film given by ID and the actor given by ID                              |
| `PUT`        | **/films/{filmId}/publisher/{publisherId}**                            | Add the relation between the film given by the ID and the publisher given by ID                         |
| `DELETE`     | **/films/{filmId}/publisher/{publisherId}**                            | Delete the relation between the film given by ID and the publisher given by ID                          |
| `PUT`        | **/films/{filmId}/director/{directorId}**                              | Add the relation between the film given by the ID and the director given by ID                          |
| `DELETE`     | **/films/{filmId}/director/{directorId}**                              | Delete the relation between the film given by ID and the director given by ID                           |
| `GET`        | **/films/sequels/{title}**                                             | Given a film by the title, find all the sequels of the film                                             |
| `GET`        | **/films/director/{directorId}**                                       | Select all the films directed by the director with the specified ID                                     |
| `GET`        | **/films/query?film_genre={genre}&director_nationality={nationality}** | Select all the films with the specified genre and directed by a director with the specified nationality |

## Software architecture
- Controller
  - ActorController
  - DirectorController
  - FilmController
  - PublisherController
- Model
  - Person
  - Actor
  - Director
  - Film
  - Publisher
- Repository
  - ActorRepository
  - FilmRepository
  - DirectorRepository
  - PersonRepository
  - PublisherRepository
- Service
  - PersonService
  - ActorService
  - DirectorService
  - FilmService
  - PublisherService

### Model package
Each model uses the annotation `@DATA`, this is an annotation provided by Lombok, and it's used to generate getters and setters automatically.

![Package model](images/package-model.png)

`Person` is an abstract class, because the application domain requires either `Actor` or `Director` to be saved in the database but not `Person`.

The classes `Actor` and `Director` inherit from `Person`. The
annotation `@Inheritance(strategy = InheritanceType.SINGLE_TABLE)` is
used in `Person`. This leads to a single table in the database using a
column `type` to distinguish between Actor and Director. This strategy
is the most efficient from a time perspective but it does however lead
to `NULL` values in the database for fields that are not shared
between the classes. This is not an issue given the fact that Actor
and Director don't have a lot of attributes that are not inherited by
Person.

In the `Film` model the prequel field is annotated with
`@JsonBackReference` in order to avoid a stack overflow error due to
infinite JSON recursion (since prequel and sequel are a recursive
relation).

### Controller package
This package contains all the controllers. The controller class is
used to receive HTTP requests.


### Repository package
The repository class is used as an interface that provides CRUD
functionalities to an entity.

Since `Actor` and `Director` share most of the CRUD functionalities,
we exploit inheritance among these repository classes.

![Package repository](images/package-repository.png)

### Service package
These class files are used to write the business logic. Likewise, as
we've done for the controller, model and repository, an abstract class
`PersonService` is created, which is extended by `DirectorService`
and `ActorService`.

![Package service](images/package-service.png)

## Tests
For testing purposes we use a different database besides the production
one. To achieve this behaviour a different `Profile` has been
created. The information regarding this aspect are contained in the
file `test/java/resources/application-test.yml`.

`PopulateDB` is a utility class. In fact, it contains only static
methods that are used to populate the database before each integration
test is run.  This step is crucial because the entire test database is
dropped after each new test execution.

In order to test the API we developed several integration tests:

- DirectorApiTest 
  - CRUD operation on Director model.
- PublisherApiTest
  - CRUD operation on Publisher model. 
- ActorApiTest 
  - CRUD operation on Actor model.
- FilmApiTest 
  - CRUD operation on Film model. 
  
### Film test
Given the relations among the films and the other entities, a create operation of a new film is managed as follows:

1. Create a new film with the *Title* and *Genre* fields:

    `POST` - `api/films/`

2. Add the actors to the film, assuming that both already exist:
  
    `PUT` - `api/films/{id}/actor/{actorId}`
    
3. Add the director to the film, assuming that they both exist:

    `PUT` - `api/films/{id}/publisher/{publisherId}`
4. Add the publisher to the film, assuming both already exist:

    `PUT` - `api/films/{id}/director/{directorId}`
5. If the film has a prequel add it, assuming both films already exist:
    
    `PUT` - `api/films/{id}/prequel/{prequelId}`
6.If the film has a sequel add it, assuming both films already exist:

    `PUT` - `api/films/{id}/sequel/{sequelId}`















