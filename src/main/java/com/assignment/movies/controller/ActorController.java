package com.assignment.movies.controller;

import com.assignment.movies.model.Actor;
import com.assignment.movies.model.Director;
import com.assignment.movies.service.ActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ActorController {

    private final ActorService actorService;

    @Autowired
    public ActorController(ActorService actorService) {
        this.actorService = actorService;
    }

    /*
     * Retrieves all actors
     * */
    @GetMapping("/actors")
    public List<Actor> findAll() {
        return actorService.findAll();
    }

    /*
     * Find actor by ID
     * */
    @GetMapping("/actors/{id}")
    public Actor findById(@PathVariable("id") long id) {
        return actorService.findById(id);
    }

    /*
     * Find actor by surname
     * */
    @GetMapping("/actors/surname/{surname}")
    public List<Actor> findBySurname(@PathVariable("surname") String surname) {
        return actorService.findBySurname(surname);
    }

    /*
     * Add new actor
     * */
    @PostMapping("/actors")
    public Actor addActor(@RequestBody Actor newActor) {
        actorService.save(newActor);
        return newActor;
    }

    @DeleteMapping("/actors/{id}")
    public void deleteActor(@PathVariable("id") long id) {
        actorService.deleteById(id);
    }

    @PutMapping("actors/{id}")
    public void updateActor(@PathVariable("id") long id, @RequestBody Actor actor) {
        actorService.update(id, actor);
    }

}

