package com.assignment.movies.controller;

import com.assignment.movies.model.Publisher;
import com.assignment.movies.service.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PublisherController {

    private final PublisherService publisherService;

    @Autowired
    public PublisherController(PublisherService publisherService) {
        this.publisherService = publisherService;
    }

    @GetMapping("/publishers")
    public List<Publisher> findAll() {
        return publisherService.findAll();
    }

    @GetMapping("/publishers/{id}")
    public Publisher findById(@PathVariable("id") long id) {
        return publisherService.findById(id);
    }

    @GetMapping("/publishers/name/{name}")
    public Publisher findByName(@PathVariable("name") String name) {
        return publisherService.findByName(name);
    }

    @PostMapping("/publishers")
    public Publisher addPublisher(@RequestBody Publisher publisher) {
        publisherService.save(publisher);
        return publisher;
    }

    @DeleteMapping("/publishers/{id}")
    public void deletePublisher(@PathVariable("id") long id) {
        publisherService.deleteById(id);
    }

    @PutMapping("publishers/{id}")
    public void updatePublisher(@PathVariable("id") long id, @RequestBody Publisher publisher) {
        publisherService.update(id, publisher);
    }

}
