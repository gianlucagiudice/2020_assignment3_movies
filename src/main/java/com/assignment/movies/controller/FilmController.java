package com.assignment.movies.controller;

import com.assignment.movies.model.Film;
import com.assignment.movies.model.Publisher;
import com.assignment.movies.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FilmController {

    private final FilmService filmService;

    @Autowired
    public FilmController(FilmService filmService) {
        this.filmService = filmService;
    }

    @GetMapping("/films")
    public List<Film> findAll() {
        return filmService.findAll();
    }

    @GetMapping("/films/{id}")
    public Film findById(@PathVariable("id") long id) {
        return filmService.findById(id);
    }

    @GetMapping("/films/title/{title}")
    public Film findByTitle(@PathVariable("title") String title) {
        return filmService.findByTitle(title);
    }

    @GetMapping("/films/sequels/{title}")
    public List<Film> getFilmSequels(@PathVariable("title") String title) {
        return filmService.findFilmSequels(title);
    }

    @PostMapping("/films")
    public Film addFilm(@RequestBody Film film) {
        return filmService.save(film);
    }

    @PutMapping("/films/{filmId}/sequel/{sequelId}")
    public Film setSequelToFilm(@PathVariable("filmId") long filmId, @PathVariable("sequelId") long sequelId) {
        return filmService.setSequelToFilm(filmId, sequelId);
    }

    @DeleteMapping("/films/{filmId}/sequel")
    public Film unsetSequelFromFilm(@PathVariable("filmId") long filmId) {
        return filmService.unsetSequelFromFilm(filmId);
    }

    @DeleteMapping("/films/{filmId}/prequel")
    public Film unsetPrequelFromFilm(@PathVariable("filmId") long filmId) {
        return filmService.unsetPrequelFromFilm(filmId);
    }

    @PutMapping("/films/{filmId}/prequel/{prequelId}")
    public Film setPrequelToFilm(@PathVariable("filmId") long filmId, @PathVariable("prequelId") long prequelId) {
        return filmService.setPrequelToFilm(filmId, prequelId);
    }

    @PutMapping("/films/{filmId}/actor/{actorId}")
    public Film addActorToFilm(@PathVariable("filmId") long filmId, @PathVariable("actorId") long actorId) {
        return filmService.addActorToFilm(filmId, actorId);
    }

    @DeleteMapping("/films/{filmId}/actor/{actorId}")
    public Film removeActorFromFilm(@PathVariable("filmId") long filmId, @PathVariable("actorId") long actorId) {
        return filmService.removeActorFromFilm(filmId, actorId);
    }

    @PutMapping("/films/{filmId}/director/{directorId}")
    public Film addDirectorToFilm(@PathVariable("filmId") long filmId, @PathVariable("directorId") long directorId) {
        return filmService.addDirectorToFilm(filmId, directorId);
    }

    @DeleteMapping("/films/{filmId}/director")
    public Film removeDirectorToFilm(@PathVariable("filmId") long filmId) {
        return filmService.removeDirectorFromFilm(filmId);
    }

    @PutMapping("/films/{filmId}/publisher/{publisherId}")
    public Film addPublisherToFilm(@PathVariable("filmId") long filmId, @PathVariable("publisherId") long publisherId) {
        return filmService.addPublisherToFilm(filmId, publisherId);
    }

    @DeleteMapping("/films/{filmId}/publisher")
    public Film removePublisherToFilm(@PathVariable("filmId") long filmId) {
        return filmService.removePublisherFromFilm(filmId);
    }

    @GetMapping("films/director/{directorId}")
    public List<Film> findFilmsByDirector(@PathVariable("directorId") long directorId) {
        return filmService.findFilmsByDirector(directorId);
    }

    @GetMapping("/films/query")
    public List<Film> findFilmsByGenreWhichAreDirectedByDirectorWithNationality(
            @RequestParam(value = "film_genre") String genre,
            @RequestParam(value = "director_nationality") String nationality){
        return filmService.findFilmsByGenreWhichAreDirectedByDirectorWithNationality(genre, nationality);
    }

    @PutMapping("films/{id}")
    public void updateFilms(@PathVariable("id") long id, @RequestBody Film film) {
        filmService.update(id, film);
    }


}
