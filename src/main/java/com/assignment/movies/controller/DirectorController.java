package com.assignment.movies.controller;

import com.assignment.movies.model.Director;
import com.assignment.movies.model.Publisher;
import com.assignment.movies.service.DirectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DirectorController {

    private final DirectorService directorService;

    @Autowired
    public DirectorController(DirectorService directorService) {
        this.directorService = directorService;
    }

    /*
    --------------- GET REQUESTS ---------------
     */
    /*
     * Retrieves all actors
     * */
    @GetMapping("/directors")
    public List<Director> findAll() {
        return directorService.findAll();
    }

    /*
     * Find actor by ID
     * */
    @GetMapping("/directors/{id}")
    public Director findById(@PathVariable("id") long id) {
        return directorService.findById(id);
    }

    /*
     * Find actor by surname
     * */
    @GetMapping("/directors/surname/{surname}")
    public List<Director> findBySurname(@PathVariable("surname") String surname) {
        return directorService.findBySurname(surname);
    }

    @PostMapping("/directors")
    public Director addDirector(@RequestBody Director director) {
        directorService.save(director);
        return director;
    }

    @DeleteMapping("/directors/{id}")
    public void deleteDirector(@PathVariable("id") long id) {
        directorService.deleteById(id);
    }

    @PutMapping("directors/{id}")
    public void updateDirectors(@PathVariable("id") long id, @RequestBody Director director) {
        directorService.update(id, director);
    }
}
