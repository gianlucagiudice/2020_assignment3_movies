package com.assignment.movies.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class Director extends Person {

    private String nationality;

    public Director(String name, String surname, String nationality) {
        super(name, surname);
        this.nationality = nationality;
    }

    public Director() {
        super();
    }

    @Override
    public String toString() {
        return "Director{" +
                "nationality='" + nationality + '\'' +
                '}';
    }
}
