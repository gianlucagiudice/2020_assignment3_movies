package com.assignment.movies.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Data
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Film {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String title;
    private String genre;

    @OneToOne(cascade = CascadeType.ALL)
    @JsonBackReference  // Avoid infinite JSON recursion
    private Film prequel;

    @OneToOne(cascade = CascadeType.ALL)
    @JsonManagedReference
    private Film sequel;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Actor> actors = new HashSet<>();

    @ManyToOne
    private Director director;

    @ManyToOne
    private Publisher publisher;

    public Film() {
        super();
    }

    public Film(String title, String genre) {
        this(title, genre, null, null, null, null, null);
    }

    public Film(String title, String genre, Film prequel, Film sequel) {
        this(title, genre, prequel, sequel, null, null, null);
    }

    public Film(String title, String genre, Film prequel, Film sequel, Set<Actor> actors,
                Director director, Publisher publisher) {
        this.title = title;
        this.genre = genre;
        this.prequel = prequel;
        this.sequel = sequel;
        this.actors = actors;
        this.director = director;
        this.publisher = publisher;
    }

    public void addActor(Actor actor) {
        this.actors.add(actor);
        actor.getActedIn().add(this);
    }

    public void removeActor(Actor actor) {
        this.actors.remove(actor);
        actor.getActedIn().remove(this);
    }

    public ArrayList<Film> findAllSequels() {
        ArrayList<Film> sequels = new ArrayList<>();
        Film sequelToAppend = sequel;
        while (sequelToAppend != null) {
            sequels.add(sequelToAppend);
            sequelToAppend = sequelToAppend.getSequel();
        }
        return sequels;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Film film = (Film) o;
        return id == film.id && title.equals(film.title) && Objects.equals(genre, film.genre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, genre);
    }
}
