package com.assignment.movies.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Actor extends Person {
    private int oscarNumber;

    @ManyToMany(mappedBy = "actors", fetch = FetchType.EAGER)
    private Set<Film> actedIn = new HashSet<>();

    public Actor(String name, String surname, int oscarNumber) {
        super(name, surname);
        this.oscarNumber = oscarNumber;
    }

    public Actor() {
        super();
    }

    @Override
    public String toString() {
        return "Actor{" +
                "Person='" + super.toString() + '\'' +
                ", oscarNumber='" + getOscarNumber() + '\'' +
                '}';
    }

}
