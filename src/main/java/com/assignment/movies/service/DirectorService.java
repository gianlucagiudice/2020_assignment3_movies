package com.assignment.movies.service;

import com.assignment.movies.model.Actor;
import com.assignment.movies.model.Director;
import com.assignment.movies.repository.DirectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DirectorService extends PersonService<Director> {

    @Autowired
    public DirectorService(DirectorRepository directorRepository) {
        super(directorRepository);
    }

    public Director update(long idDirector, Director directorUpdate)
    {
        Director director = super.update(idDirector,directorUpdate);
        director.setNationality(directorUpdate.getNationality());
        return save(director);
    }

}
