package com.assignment.movies.service;

import com.assignment.movies.model.Actor;
import com.assignment.movies.repository.ActorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActorService extends PersonService<Actor> {

    @Autowired
    public ActorService(ActorRepository actorRepository) {
        super(actorRepository);
    }

    public Actor update(long idActor, Actor actorUpdate)
    {
        Actor actor = super.update(idActor,actorUpdate);
        actor.setOscarNumber(actorUpdate.getOscarNumber());
        return save(actor);
    }

}
