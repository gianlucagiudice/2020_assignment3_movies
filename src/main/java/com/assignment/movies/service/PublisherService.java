package com.assignment.movies.service;

import com.assignment.movies.model.Publisher;
import com.assignment.movies.repository.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PublisherService {

    private final PublisherRepository publisherRepository;

    @Autowired
    public PublisherService(PublisherRepository publisherRepository) {
        this.publisherRepository = publisherRepository;
    }

    public Publisher findById(long id) {
        return publisherRepository.findById(id);
    }

    public Publisher findByName(String name) {
        return publisherRepository.findByName(name);
    }

    public List<Publisher> findAll() {
        return publisherRepository.findAll();
    }

    public Publisher save(Publisher publisher) {
        return publisherRepository.save(publisher);
    }

    public void deleteById(long id) {
        publisherRepository.deleteById(id);
    }

    public Publisher update(long id, Publisher publisherUpdate){
        Publisher publisher = publisherRepository.findById(id);
        publisher.setName(publisherUpdate.getName());
        return publisherRepository.save(publisher);
    }
}
