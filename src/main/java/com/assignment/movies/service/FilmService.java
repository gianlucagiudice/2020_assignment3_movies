package com.assignment.movies.service;

import com.assignment.movies.model.Actor;
import com.assignment.movies.model.Director;
import com.assignment.movies.model.Film;
import com.assignment.movies.model.Publisher;
import com.assignment.movies.repository.ActorRepository;
import com.assignment.movies.repository.DirectorRepository;
import com.assignment.movies.repository.FilmRepository;
import com.assignment.movies.repository.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilmService {

    private final FilmRepository filmRepository;
    private final PublisherRepository publisherRepository;
    private final DirectorRepository directorRepository;
    private final ActorRepository actorRepository;

    @Autowired
    public FilmService(FilmRepository filmRepository, PublisherRepository publisherRepository,
                       DirectorRepository directorRepository, ActorRepository actorRepository) {
        this.filmRepository = filmRepository;
        this.publisherRepository = publisherRepository;
        this.directorRepository = directorRepository;
        this.actorRepository = actorRepository;
    }

    public Film findById(long id) {
        return filmRepository.findById(id);
    }

    public Film findByTitle(String title) {
        return filmRepository.findByTitle(title);
    }

    public List<Film> findFilmSequels(String title) {
        return filmRepository.findByTitle(title).findAllSequels();
    }

    public List<Film> findAll() {
        return filmRepository.findAll();
    }

    public Film save(Film film) {
        return filmRepository.save(film);
    }

    public Film setSequelToFilm(long filmId, long sequelId) {
        Film film = filmRepository.findById(filmId);
        Film sequel = filmRepository.findById(sequelId);

        film.setSequel(sequel);
        sequel.setPrequel(film);
        save(sequel);
        return save(film);
    }

    public Film unsetSequelFromFilm(long filmId) {
        Film film = filmRepository.findById(filmId);
        Film sequel = filmRepository.findById(film.getSequel().getId());

        film.setSequel(null);
        sequel.setPrequel(null);
        save(sequel);
        return save(film);
    }

    public Film setPrequelToFilm(long filmId, long prequelId) {
        Film film = filmRepository.findById(filmId);
        Film prequel = filmRepository.findById(prequelId);

        film.setPrequel(prequel);
        prequel.setSequel(film);
        save(prequel);
        return save(film);
    }

    public Film unsetPrequelFromFilm(long filmId) {
        Film film = filmRepository.findById(filmId);
        Film prequel = filmRepository.findById(film.getPrequel().getId());

        film.setPrequel(null);
        prequel.setSequel(null);
        save(prequel);
        return save(film);
    }

    public Film addActorToFilm(long filmId, long actorId) {
        Film film = filmRepository.findById(filmId);
        Actor actor = actorRepository.findById(actorId);
        film.addActor(actor);
        return save(film);
    }

    public Film removeActorFromFilm(long filmId, long actorId) {
        Film film = filmRepository.findById(filmId);
        Actor actor = actorRepository.findById(actorId);

        film.removeActor(actor);
        return save(film);
    }

    public Film addDirectorToFilm(long filmId, long directorId) {
        Film film = filmRepository.findById(filmId);
        Director director = directorRepository.findById(directorId);

        film.setDirector(director);
        return save(film);
    }

    public Film removeDirectorFromFilm(long filmId) {
        Film film = filmRepository.findById(filmId);
        film.setDirector(null);
        return save(film);
    }

    public Film addPublisherToFilm(long filmId, long publisherId) {
        Film film = filmRepository.findById(filmId);
        Publisher publisher = publisherRepository.findById(publisherId);

        film.setPublisher(publisher);
        return save(film);
    }

    public Film removePublisherFromFilm(long filmId) {
        Film film = filmRepository.findById(filmId);
        film.setPublisher(null);
        return save(film);
    }

    public Film update(long filmId, Film filmUpdate) {
        Film film = filmRepository.findById(filmId);
        film.setTitle(filmUpdate.getTitle());
        film.setGenre(filmUpdate.getGenre());
        return filmRepository.save(film);
    }

    public List<Film> findFilmsByDirector(long directorId) {
        return filmRepository.findFilmsByDirector(directorId);
    }

    public List<Film> findFilmsByGenreWhichAreDirectedByDirectorWithNationality(String genre, String nationality){
        return filmRepository.findFilmsByGenreWhichAreDirectedByDirectorWithNationality(genre, nationality);
    }


}
