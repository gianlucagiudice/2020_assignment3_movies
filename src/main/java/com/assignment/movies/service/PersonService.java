package com.assignment.movies.service;

import com.assignment.movies.model.Person;
import com.assignment.movies.repository.PersonRepository;

import java.util.List;

public abstract class PersonService<T extends Person> {

    private final PersonRepository<T> personRepository;

    public PersonService(PersonRepository<T> personRepository) {
        this.personRepository = personRepository;
    }

    public T findById(long id) {
        return personRepository.findById(id);
    }

    public List<T> findBySurname(String surname) {
        return personRepository.findBySurname(surname);
    }

    public T save(T person) {
        return personRepository.save(person);
    }

    public List<T> findAll() {
        return (List<T>) personRepository.findAll();
    }

    public void deleteById(long id) {
        personRepository.deleteById(id);
    }

    public T update(long personId,T personUpdate) {
        T person = personRepository.findById(personId);
        person.setName(personUpdate.getName());
        person.setSurname(personUpdate.getSurname());
        return personRepository.save(person);
    }
}
