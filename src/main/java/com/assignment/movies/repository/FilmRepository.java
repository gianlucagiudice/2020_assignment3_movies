package com.assignment.movies.repository;

import com.assignment.movies.model.Film;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FilmRepository extends JpaRepository<Film, Long> {
    Film findById(long id);

    Film findByTitle(String title);

    @Query("SELECT f FROM Film f WHERE f.director.id = ?1")
    List<Film> findFilmsByDirector(long directorId);

    @Query( "SELECT DISTINCT f " +
            "FROM Film f, Director d " +
            "WHERE f.genre = ?1 AND d.nationality = ?2")
    List<Film> findFilmsByGenreWhichAreDirectedByDirectorWithNationality(String genre, String nationality);
}
