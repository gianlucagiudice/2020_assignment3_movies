package com.assignment.movies.repository;

import com.assignment.movies.model.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Long> {
    Publisher findById(long id);

    Publisher findByName(String name);
}
