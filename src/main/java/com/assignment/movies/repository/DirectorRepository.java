package com.assignment.movies.repository;

import com.assignment.movies.model.Director;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectorRepository extends PersonRepository<Director> {
}
