package com.assignment.movies.repository;

import com.assignment.movies.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface PersonRepository<T extends Person> extends JpaRepository<Person, Long> {
    T findById(long id);

    List<T> findBySurname(String surname);
}
