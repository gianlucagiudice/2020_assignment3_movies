package com.assignment.movies.repository;

import com.assignment.movies.model.Actor;
import org.springframework.stereotype.Repository;

@Repository
public interface ActorRepository extends PersonRepository<Actor> {
}
