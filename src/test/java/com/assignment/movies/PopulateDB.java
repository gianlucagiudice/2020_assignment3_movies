    package com.assignment.movies;

    import com.assignment.movies.model.Actor;
    import com.assignment.movies.model.Director;
    import com.assignment.movies.model.Film;
    import com.assignment.movies.model.Publisher;
    import com.assignment.movies.service.ActorService;
    import com.assignment.movies.service.DirectorService;
    import com.assignment.movies.service.FilmService;
    import com.assignment.movies.service.PublisherService;

    public class PopulateDB {

        private static final String[][] INIT_ACTORS_DATA = {
            {"Robin", "Williams", "4"},
            {"Harrison", "Ford", "3"},
            {"Sandra", "Bullock", "9"},
            {"Anya", "Taylor-Joy", "15"}
        };

        private static final String[][] INIT_DIRECTORS_DATA = {
            {"Lana", "Wachowski", "United States"},
            {"Quentin", "Tarantino", "United States"}
        };

        private static final String[] INIT_PUBLISHERS_DATA = {
            "Warner Bros",
            "20th Century Fox",
            "Paramount Pictures",
            "Lionsgate Films",
            "DreamWorks Pictures",
        };

        public static void initActors(ActorService service){
            for (String[] row : INIT_ACTORS_DATA){
                service.save(new Actor(row[0], row[1], Integer.parseInt(row[2])));
            }
        }

        public static void initDirectors(DirectorService service){
            for (String[] row : INIT_DIRECTORS_DATA){
                service.save(new Director(row[0], row[1], row[2]));
            }
        }

        public static void initPublishers(PublisherService service){
            for (String publisher : INIT_PUBLISHERS_DATA){
                service.save(new Publisher(publisher));
            }
        }

        public static void initFilms(FilmService filmService){
            Film matrix1 = new Film("Matrix", "sci-fi");
            Film matrix2 = new Film("Matrix Reloaded", "sci-fi", matrix1, null);
            Film matrix3 = new Film("Matrix Revolutions", "sci-fi", matrix2, null);
            matrix1.setSequel(matrix2);
            matrix2.setSequel(matrix3);

            filmService.save(matrix1);
            filmService.save(matrix2);
            filmService.save(matrix3);
            filmService.save(new Film("The Hateful Eight", "Drama", null, null, null, null, null));

            Actor actorTest = new Actor("Andrew","Robinson", 0);
            Film filmTest = new Film("Hellraiser 2", "Horror", null, null, null, null, null);
            filmService.save(filmTest);
        }
    }
