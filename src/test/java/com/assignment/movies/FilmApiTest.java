package com.assignment.movies;

import com.assignment.movies.model.Actor;
import com.assignment.movies.model.Director;
import com.assignment.movies.model.Film;
import com.assignment.movies.model.Publisher;
import com.assignment.movies.service.ActorService;
import com.assignment.movies.service.DirectorService;
import com.assignment.movies.service.FilmService;
import com.assignment.movies.service.PublisherService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = {MoviesApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class FilmApiTest {

    @Autowired
    private MockMvc mvc;

    @BeforeAll
    static void initFilms(@Autowired FilmService filmService, @Autowired ActorService actorService,
                          @Autowired DirectorService directorService){
        PopulateDB.initFilms(filmService);
        PopulateDB.initActors(actorService);
        PopulateDB.initDirectors(directorService);
    }

    /*
     * Find all films
     */
    @Test
    void getAllFilms(@Autowired FilmService service) throws Exception {
        MvcResult result = mvc.perform(get("/films"))
                .andExpect(status().is(HttpStatus.OK.value())).andReturn();

        String json = result.getResponse().getContentAsString();

        ObjectMapper mapper = new ObjectMapper();
        Film[] films = mapper.readValue(json, Film[].class);

        assertEquals((service.findAll()).size(), films.length);
    }


    /*
     * Find all sequel of a film given the title
     */
    @Test
    void getSequelsByTitle() throws Exception {
        // Last film in the saga
        String filmInSaga = "Matrix Reloaded";

        // Get saga by title
        MvcResult result = mvc.perform(get("/films/sequels/" + filmInSaga))
                .andExpect(status().is(HttpStatus.OK.value())).andReturn();

        String json = result.getResponse().getContentAsString();

        // Films of the saga found
        ObjectMapper mapper = new ObjectMapper();
        Film[] films = mapper.readValue(json, Film[].class);

        // Assert the saga is correct
        assertEquals("Matrix Revolutions", films[0].getTitle());
    }

    @Test
    void addNewFilm(@Autowired FilmService service) throws Exception {
        String film = "{title: \"The Lighthouse\", genre: \"Horror\"}";
        JSONObject jsonFilm = new JSONObject(film);

        // Post request to add new film
        mvc.perform(
                post("/films")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(String.valueOf(jsonFilm)))
                .andExpect(status().isOk())
                .andReturn();

        // Check if the new Film has been added
        assertNotNull(service.findByTitle("The Lighthouse"));
    }

    @Test
    void setSequelToFilm(@Autowired FilmService service) throws Exception {
        Film film = new Film("Blade Runner", "Sci-fi");
        Film sequel = new Film("Blade Runner 2049", "Sci-fi");

        service.save(film);
        service.save(sequel);

        mvc.perform(
                put("/films/" + film.getId() +
                                "/sequel/" + sequel.getId()))
                .andExpect(status().isOk())
                .andReturn();

        // check sequel
        assertEquals(service.findById(film.getId()).getSequel().getId(), sequel.getId());
        // check if sequel has film as prequel
        assertEquals(service.findById(sequel.getId()).getPrequel().getId(), film.getId());

    }

    @Test
    void unsetSequelFromFilm(@Autowired FilmService service) throws Exception {
        Film film = new Film("Blade Runner", "Sci-fi");
        Film sequel = new Film("Blade Runner 2049", "Sci-fi");

        film.setSequel(sequel);
        sequel.setPrequel(film);
        service.save(film);
        service.save(sequel);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.delete("/films/" + film.getId() + "/sequel/"))
                .andExpect(status().is(HttpStatus.OK.value())).andReturn();

        // check sequel
        assertNull(service.findById(film.getId()).getSequel());
        // check if sequel has film as prequel
        assertNull(service.findById(sequel.getId()).getPrequel());

    }

    @Test
    void setPrequelToFilm(@Autowired FilmService service) throws Exception {
        Film film = new Film("Aliens", "Sci-fi");
        Film prequel = new Film("Alien", "Sci-fi");

        service.save(film);
        service.save(prequel);

        MvcResult result = mvc.perform(
                put("/films/" + film.getId() +
                        "/prequel/" + prequel.getId()))
                .andExpect(status().isOk())
                .andReturn();

        // check prequel
        assertEquals(service.findById(film.getId()).getPrequel().getId(), prequel.getId());
        // check if prequel has film as sequel
        assertEquals(service.findById(prequel.getId()).getSequel().getId(), film.getId());

    }

    @Test
    void unsetPrequelFromFilm(@Autowired FilmService service) throws Exception {
        Film film = new Film("Terminator 2: Judgment Day", "Sci-fi");
        Film prequel = new Film("The Terminator", "Sci-fi");

        film.setPrequel(prequel);
        prequel.setSequel(film);
        service.save(film);
        service.save(prequel);

        mvc.perform(MockMvcRequestBuilders.delete("/films/" + film.getId() + "/prequel/"))
                .andExpect(status().is(HttpStatus.OK.value())).andReturn();

        // check prequel
        assertNull(service.findById(film.getId()).getPrequel());
        assertNull(service.findById(prequel.getId()).getSequel());

    }

    @Test
    void addActorToFilm(@Autowired FilmService filmService, @Autowired ActorService actorService) throws Exception {
        Film film = new Film("Her", "Sci-fi");
        Actor actor = new Actor("Joaquin","Phoenix", 10);

        actorService.save(actor);
        filmService.save(film);

        MvcResult result = mvc.perform(
                put("/films/" + film.getId() +
                        "/actor/" + actor.getId()))
                .andExpect(status().isOk())
                .andReturn();

        String json = result.getResponse().getContentAsString();

        // se gli passassi actor sarebbe sbagliato
        // devo prendere actor dal DB perché solo se passa per il DB gli vengono aggiornate le relazioni
        assertTrue(filmService.findById(film.getId()).getActors().contains(actorService.findById(actor.getId())));
    }

    @Test
    void removeActorFromFilm(@Autowired FilmService filmService, @Autowired ActorService actorService) throws Exception {
        Film film = new Film("Gladiator", "Action");
        Actor actor = new Actor("Russell","Crowe", 0);

        actorService.save(actor);
        filmService.save(film);
        film = filmService.findById(film.getId());
        film.addActor(actorService.findById(actor.getId()));
        film = filmService.save(film);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.delete("/films/" + film.getId() +
                                                    "/actor/" + actor.getId()))
                .andExpect(status().is(HttpStatus.OK.value())).andReturn();

        assertFalse(filmService.findById(film.getId()).getActors().contains(actorService.findById(actor.getId())));
    }

    @Test
    void addDirectorToFilm(@Autowired FilmService filmService, @Autowired DirectorService directorService) throws Exception {
        Film film = new Film("The VVitch", "Horror");
        Director director = new Director("Robert","Eggers", "United States");

        directorService.save(director);
        filmService.save(film);

        MvcResult result = mvc.perform(
                put("/films/" + film.getId() +
                        "/director/" + director.getId()))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(filmService.findById(film.getId()).getDirector(), directorService.findById(director.getId()));
    }

    @Test
    void removeDirectorFromFilm(@Autowired FilmService filmService, @Autowired DirectorService directorService) throws Exception {
        Film film = new Film("Possession", "Horror");
        Director director = new Director("Andrzej", "Żuławski", "Ukranian");

        film.setDirector(director);
        directorService.save(director);
        filmService.save(film);

        mvc.perform(MockMvcRequestBuilders.delete("/films/" + film.getId() + "/director/"))
                .andExpect(status().is(HttpStatus.OK.value())).andReturn();

        assertNull(filmService.findById(film.getId()).getDirector());
    }

    @Test
    void addPublisherToFilm(@Autowired FilmService filmService,
                            @Autowired PublisherService publisherService) throws Exception {
        Publisher publisher = publisherService.save(new Publisher("Walt Disney Studios"));
        Film film = filmService.save(new Film("Toy story", "Adventure"));

        mvc.perform(
                put("/films/" + film.getId() +
                        "/publisher/" + publisher.getId()))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(filmService.findById(film.getId()).getPublisher(), publisherService.findById(publisher.getId()));
    }

    @Test
    void removePublisherFromFilm(@Autowired FilmService filmService, @Autowired PublisherService publisherService) throws Exception {
        Film film = new Film("Cinderella", "Drama");
        Publisher publisher = new Publisher("Fans only");
        film.setPublisher(publisher);
        publisherService.save(publisher);
        filmService.save(film);

        mvc.perform(MockMvcRequestBuilders.delete("/films/" + film.getId() + "/director/"))
                .andExpect(status().isOk())
                .andReturn();

        assertNull(filmService.findById(film.getId()).getDirector());
    }

    @Test
    void findFilmsByDirector(@Autowired FilmService filmService, @Autowired DirectorService directorService) throws Exception {
        Film film = new Film("Seven", "Thriller");
        Film film2 = new Film("Zodiac", "Thriller");
        Director director = new Director("David", "Fincher", "United States");

        film.setDirector(director);
        film2.setDirector(director);
        directorService.save(director);
        filmService.save(film);
        filmService.save(film2);

        MvcResult result = mvc.perform(
                get("/films/director/" + director.getId()))
                .andExpect(status().isOk())
                .andReturn();

        String json = result.getResponse().getContentAsString();

        ObjectMapper mapper = new ObjectMapper();
        Film[] foundFilms = mapper.readValue(json, Film[].class);

        assertEquals(foundFilms.length,2);

        Set<Film> filmSet = new HashSet<>();

        Collections.addAll(filmSet, foundFilms);

        assertTrue(filmSet.contains(film));
        assertTrue(filmSet.contains(film2));
    }

    @Test
    void findFilmsByGenreWhichAreDirectedByDirectorWithNationality(
            @Autowired FilmService filmService, @Autowired DirectorService directorService) throws Exception {
        Film film1 = new Film("Grand Budapest Hotel", "Thriller");
        Film film2 = new Film("Borat Subsequent Moviefilm", "Comedy");
        Director director1 = new Director("Wes", "Anderson", "United States");
        Director director2 = new Director("Jason", "Woliner", "United States");

        film1.setDirector(director1);
        film2.setDirector(director2);

        directorService.save(director1);
        directorService.save(director2);
        filmService.save(film1);
        filmService.save(film2);

        String parameters = "?film_genre=Thriller&director_nationality=United States";
        MvcResult result = mvc.perform(
                get("/films/query" + parameters))
                .andExpect(status().isOk())
                .andReturn();

        String json = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        Film[] foundFilms = mapper.readValue(json, Film[].class);

        Set<Film> filmSet = new HashSet<>();
        Collections.addAll(filmSet, foundFilms);

        assertTrue(filmSet.contains(film1));
        assertFalse(filmSet.contains(film2));
    }

    @Test
    void updateFilm(@Autowired FilmService service) throws Exception {
        Film film = service.save(new Film("Mad Max", "Action"));
        long id = film.getId();

        String pub = "{title: \"The Thing\", genre: \"Horror\"}";
        JSONObject jsonPub = new JSONObject(pub);

        mvc.perform(put("/films/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(String.valueOf(jsonPub)))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(service.findById(id).getTitle(), "The Thing");
        assertEquals(service.findById(id).getGenre(), "Horror");
    }


}
