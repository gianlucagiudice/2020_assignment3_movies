package com.assignment.movies;

import com.assignment.movies.model.Actor;
import com.assignment.movies.model.Director;
import com.assignment.movies.service.ActorService;
import com.assignment.movies.service.DirectorService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = {MoviesApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ActorApiTest {

    @Autowired
    private MockMvc mvc;

    @BeforeAll
    static void initActors(@Autowired ActorService service){
        PopulateDB.initActors(service);
    }

    @Test
    void getAllActors(@Autowired ActorService service) throws Exception {
        MvcResult result = mvc.perform(get("/actors"))
                .andExpect(status().is(HttpStatus.OK.value())).andReturn();

        String json = result.getResponse().getContentAsString();

        ObjectMapper mapper = new ObjectMapper();
        Actor[] actors = mapper.readValue(json, Actor[].class);

        assertEquals(service.findAll().size(), actors.length);
    }

    @Test
    void findActorById(@Autowired ActorService service) throws Exception {
        Actor actorTarget = service.findAll().iterator().next();

        MvcResult result = mvc.perform(get("/actors/" + actorTarget.getId()))
                .andExpect(status().is(HttpStatus.OK.value())).andReturn();

        String json = result.getResponse().getContentAsString();

        ObjectMapper mapper = new ObjectMapper();
        Actor actorAPI = mapper.readValue(json, Actor.class);

        assertEquals(actorTarget, actorAPI);
    }

    @Test
    void findActorBySurname(@Autowired ActorService service) throws Exception {
        String surname = "Hanks";
        Actor[] actorsTarget = service.findBySurname(surname).toArray(new Actor[0]);

        MvcResult result = mvc.perform(get("/actors/surname/" + surname))
                .andExpect(status().is(HttpStatus.OK.value())).andReturn();

        String json = result.getResponse().getContentAsString();

        ObjectMapper mapper = new ObjectMapper();
        Actor[] actorAPI = mapper.readValue(json, Actor[].class);

        // Must return the same actors
        for(int i = 0; i < actorAPI.length; i++){
            assertEquals(actorsTarget[i], actorAPI[i]);
        }
    }

    @Test
    void addNewActor(@Autowired ActorService service) throws Exception {
        String actor = "{name: \"Tom\", surname: \"Hanks\", oscarNumber: \"5\"}";
        JSONObject jsonActor = new JSONObject(actor);

        // Post request to add new actor
        mvc.perform(
                post("/actors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(String.valueOf(jsonActor)))
                .andExpect(status().isOk())
                .andReturn();

        // Check if the new actor has been added
        assertNotNull(service.findBySurname("Hanks"));
    }

    @Test
    void deleteActor(@Autowired ActorService service) throws Exception {
        Actor actorToDelete = service.save(new Actor("Actor", "Delete", 0));

        long id = actorToDelete.getId();

        mvc.perform(MockMvcRequestBuilders.delete("/actors/" + id))
                .andExpect(status().is(HttpStatus.OK.value())).andReturn();

        assertNull(service.findById(id));
    }

    @Test
    void updateActor (@Autowired ActorService service) throws Exception {
        Actor actor = service.save(new Actor("Gianni", "Gianno", 0));
        long id = actor.getId();

        String act = "{name: \"Maccio\", surname: \"Capatonda\", oscarNumber: \"3\"}";
        JSONObject jsonAct = new JSONObject(act);

        mvc.perform(put("/actors/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(String.valueOf(jsonAct)))
                .andExpect(status().isOk())
                .andReturn();

        assertNotEquals(service.findById(id).getName(), actor.getName());
        assertNotEquals(service.findById(id).getSurname(), actor.getSurname());
        assertNotEquals(service.findById(id).getOscarNumber(), actor.getOscarNumber());
    }

}
