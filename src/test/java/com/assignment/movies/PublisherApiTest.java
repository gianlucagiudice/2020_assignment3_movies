package com.assignment.movies;

import com.assignment.movies.model.Publisher;
import com.assignment.movies.service.PublisherService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = {MoviesApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class PublisherApiTest {

    @Autowired
    private MockMvc mvc;

    @BeforeAll
    static void initActors(@Autowired PublisherService service){
        PopulateDB.initPublishers(service);
    }

    @Test
    void getAllPublishers(@Autowired PublisherService service) throws Exception {
        MvcResult result = mvc.perform(get("/publishers"))
                .andExpect(status().is(HttpStatus.OK.value())).andReturn();

        String json = result.getResponse().getContentAsString();

        ObjectMapper mapper = new ObjectMapper();
        Publisher[] publishers = mapper.readValue(json, Publisher[].class);

        assertEquals(service.findAll().size(), publishers.length);
    }


    @Test
    void findPublisherById(@Autowired PublisherService service) throws Exception {
        Publisher publisherTarget = service.findAll().iterator().next();

        MvcResult result = mvc.perform(get("/publishers/" + publisherTarget.getId()))
                .andExpect(status().is(HttpStatus.OK.value())).andReturn();

        String json = result.getResponse().getContentAsString();

        ObjectMapper mapper = new ObjectMapper();
        Publisher publisherAPI = mapper.readValue(json, Publisher.class);

        assertEquals(publisherTarget, publisherAPI);
    }

    @Test
    void findPublisherByName(@Autowired PublisherService service) throws Exception{
        String name = "Warner Bros";
        Publisher publisherTarget = service.findByName(name);

        MvcResult result = mvc.perform(get("/publishers/name/" + name))
                .andExpect(status().is(HttpStatus.OK.value())).andReturn();

        String json = result.getResponse().getContentAsString();

        ObjectMapper mapper = new ObjectMapper();
        Publisher publisherAPI = mapper.readValue(json, Publisher.class);

        assertEquals(publisherTarget, publisherAPI);
    }

    @Test
    void addNewPublisher(@Autowired PublisherService service) throws Exception {
        String publisher = "{name: \"Universal Pictures\"}";
        JSONObject jsonPublisher = new JSONObject(publisher);

        // Post request to add new actor
        mvc.perform(
                post("/publishers")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(String.valueOf(jsonPublisher)))
                .andExpect(status().isOk())
                .andReturn();

        // Check if the new actor has been added
        assertNotNull(service.findByName("Universal Pictures"));
    }

    @Test
    void deletePublisher(@Autowired PublisherService service) throws Exception {
        Publisher publisherToDelete = service.save(new Publisher("Publisher"));
        long id = publisherToDelete.getId();

        mvc.perform(MockMvcRequestBuilders.delete("/publishers/" + id))
                .andExpect(status().is(HttpStatus.OK.value())).andReturn();

        assertNull(service.findById(id));
    }

    @Test
    void updatePublisher(@Autowired PublisherService service) throws Exception {
        Publisher publisher = service.save(new Publisher("Boh"));
        long id = publisher.getId();

        String pub = "{name: \"Cambio\"}";
        JSONObject jsonPub = new JSONObject(pub);

        mvc.perform(put("/publishers/" + id)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(String.valueOf(jsonPub)))
                    .andExpect(status().isOk())
                .andReturn();

        assertEquals(service.findById(id).getName(), "Cambio");
    }

}
