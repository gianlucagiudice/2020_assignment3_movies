package com.assignment.movies;

import com.assignment.movies.model.Director;
import com.assignment.movies.model.Publisher;
import com.assignment.movies.service.DirectorService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = {MoviesApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class DirectorApiTest {

    @Autowired
    private MockMvc mvc;

    @BeforeAll
    static void initDirectors(@Autowired DirectorService service){
        PopulateDB.initDirectors(service);
    }

    @Test
    void getAllDirectors(@Autowired DirectorService service) throws Exception {
        MvcResult result = mvc.perform(get("/directors"))
                .andExpect(status().is(HttpStatus.OK.value())).andReturn();

        String json = result.getResponse().getContentAsString();

        ObjectMapper mapper = new ObjectMapper();
        Director[] directors = mapper.readValue(json, Director[].class);

        assertEquals(service.findAll().size(), directors.length);
    }

    @Test
    void findDirectorById(@Autowired DirectorService service) throws Exception {
        Director directorTarget = service.findAll().iterator().next();

        MvcResult result = mvc.perform(get("/directors/" + directorTarget.getId()))
                .andExpect(status().is(HttpStatus.OK.value())).andReturn();

        String json = result.getResponse().getContentAsString();

        ObjectMapper mapper = new ObjectMapper();
        Director directorApi = mapper.readValue(json, Director.class);

        assertEquals(directorTarget, directorApi);
    }

    @Test
    void findDirectorBySurname(@Autowired DirectorService service) throws Exception {
        String surname = "Tarantino";
        Director[] directorTarget = service.findBySurname(surname).toArray(new Director[0]);

        MvcResult result = mvc.perform(get("/directors/surname/" + surname))
                .andExpect(status().is(HttpStatus.OK.value())).andReturn();

        String json = result.getResponse().getContentAsString();

        ObjectMapper mapper = new ObjectMapper();
        Director[] directorApi = mapper.readValue(json, Director[].class);

        // Must return the same actors
        for(int i = 0; i < directorApi.length; i++){
            assertEquals(directorTarget[i], directorApi[i]);
        }

    }


    @Test
    void addNewDirector(@Autowired DirectorService service) throws Exception {
        String director = "{name: \"Denis\", surname: \"Villneuve\"}";
        JSONObject jsonDirector = new JSONObject(director);

        // Post request to add new actor
        MvcResult result = mvc.perform(
                post("/directors")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(String.valueOf(jsonDirector)))
                .andExpect(status().isOk())
                .andReturn();

        // Check if the new actor has been added
        assertNotNull(service.findBySurname("Villneuve"));
    }

    @Test
    void deleteDirector(@Autowired DirectorService service) throws Exception {
        Director directorToDelete = new Director("John", "Carpenter", "USA");
        service.save(directorToDelete);

        long id = directorToDelete.getId();

        mvc.perform(MockMvcRequestBuilders.delete("/directors/" + id))
                .andExpect(status().is(HttpStatus.OK.value())).andReturn();

        assertNull(service.findById(id));
    }

    @Test
    void updateDirector (@Autowired DirectorService service) throws Exception {
        Director director = service.save(new Director("Franco", "Franchi", "ITA"));
        long id = director.getId();

        String dir = "{name: \"Steven\", surname: \"Spielberg\", nationality: \"USA\"}";
        JSONObject jsonPub = new JSONObject(dir);

        mvc.perform(put("/directors/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(String.valueOf(jsonPub)))
                .andExpect(status().isOk())
                .andReturn();

        assertNotEquals(service.findById(id).getName(), director.getName());
        assertNotEquals(service.findById(id).getSurname(), director.getSurname());
        assertNotEquals(service.findById(id).getNationality(), director.getNationality());
    }
}
